+++
description = "Monthly Meetups"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "Monthly Meetups"
date= 2019-07-17T12:49:59+02:00
images = [
  "https://external-preview.redd.it/5XIINSlJY3jjjAjwZBg8LIaT8JE_sO_xLQFmIYxC3vU.jpg?auto=webp&s=50dc677476bdee0e0a6b8d38117de40ee465ad00"
] # overrides the site-wide open graph image
+++

# Monthly Hispagatos meetups at the CNT Anarcho Syndicalist space in Alicante, Spain

<center><img src="/images/a-m-anarchohacker-manifesto-2018-2.png" width="100%" height="100%"></center>

## What we will do with this meetings:


Group of activists focused on decentralization, freedom and privacy in the network.

Goals:
- Participate in politics at local and national level to push processes that promote the focus of the group.
- Promote the technologies and customs of the hacker culture like privacy, free software, federated or decentralized applications against centralized applications and services.
- Create a local space where we can organize our political actions and create tools for other groups of social justice activists.
- Create an Anarchist policy within the group with horizontal participation, consensus and respect

## Details:

- I'm still kind of putting things in order, and in August I'll be visiting hackerlabs / hackerspaces and CCC camp 2019 in Germany, so when we get back we can start organizing this. Bring ideas, and good vibes, BYOD bring your own drink if you want to drink, soda, water, beer, wine etc ...
- Brings your laptop/computer so we can hack on ideas and come to consensus on different topics.
- Everyone with a rebel spirit, disinfluencer, and that supports the hacker ethics and anarchist values is welcome.
- Everyone that feels they are in the wrong side of history but will like to change this is also welcome, but you must come clean.
- Police, Military, Civil Guardians(Guardia Civil) any person from an governmental or other type of totalitarian or oppressive organization, fascist, neonazis, capitalists, corporate assholes, entrepreneurs, influences, infosec and cyber-security brainwash folks working for any of the mentioned totalitarian forces or wanting to work for them,  and such you not welcome.
- Newbees, noobs and lamers are welcome but FORGET everything you have learn such most likely it was for cybersecurity, washup hacking morals  and to work/preare you  for  a capitalist system, not really hacking and hacking culture.

## More information and to reserve your sit.
- [reserve here](https://www.meetup.com/es-ES/Hispagatos-descentralizacion-libertad-y-privacidad/events/262967416/)
- [information of event](https://www.meetup.com/es-ES/Hispagatos-descentralizacion-libertad-y-privacidad/)

## Note: we will leave meetup.com since we DO NOT support those ideas and companies, this is temporary.

- [ReK2](https://keybase.io/rek2)
- [hispagatos.org](https://hispagatos.org)
- [The Anarcho Hacker Manifesto](https://theanarchistlibrary.org/library/anarchohacker-manifesto-2018)
- [Anarchist Hackers](https://anarcho-hacker.info)
