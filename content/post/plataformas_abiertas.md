---
title: "Plataformas_abiertas"
date: 2021-01-29T18:51:28+01:00
draft: false
---

## Otra vez la importancia de las  plataformas abiertas,

Los eventos que hemos visto estos dias atras de la censura contra foros tal como de /r/wallstreetbets nos enseñan, una vez más, sobre el valor de las plataformas abiertas y el tremendo riesgo que implica el uso de plataformas propietarias. Las élites económicas del capitalismo de la vigilancia que controlan esas plataformas propietarias, respaldadas por sus intereses de capital de riesgo, y el neoliberalismo viejo, siempre nos cerraran y censuraran una y otra vez. Arriesgamos mucho al poner nuestra suerte y nuestras ideas en sus manos.

Discord, una plataforma patentada de mensajería instantánea y VoIP, censuro y expulso ayer a la comunidad /r/WSB. Con el pretexto y afirmacion de que se debía al spam y al abuso de los bots. Estas son excusas muy convenientes cuando se considera el contexto más amplio del conflicto de intereses de Discord, entre sus usuarios inversores minoristas y sus patrocinadores inversores de Wall Street. Sin embargo, incluso si tomamos su declaracion al pie de la letra, podemos cuestionar fácilmente las políticas draconianas de Discord sobre su protocolo de chat propietario. Tienen un historial de tomar enormes medidas  contra bots y clientes de terceros con las mismas excusas de prevenir el spam y el abuso. Si Discord acepta la responsabilidad de prevenir el spam y el abuso, entonces ¿por qué están degradando a los usuarios cuando ellos, Discord, no pueden evitarlo?

Todo es mentira, una farsa como tantas otras. Utilizan un protocolo propietario y toman enormes medidas  contra las implementaciones de terceros porque exigen un control total sobre sus usuarios. Desplazaron el foro /r/WSB porque estaban amenazados económicamente por ellos. Discord actúa en sus propios intereses, incluso  en contra de los intereses de sus usuarios. En palabras de Rohan Kumar, están tratando de domesticar a sus usuarios. Lo mismo ocurre con todas las plataformas operadas por empresas. ¡Apostar a que Reddit finalmente cerrará /r/WSB es probablemente una apuesta más fuerte que comprar GME!

Pero hay otras opciones: plataformas, protocolos y estándares libres y abiertos. En lugar de Discord, se podría recomendar Matrix, IRC o Mumble. Estos no se basan en la propiedad empresarial central, sino en estándares disponibles públicamente sobre los que cualquiera puede construir. La propiedad de estas plataformas se distribuye entre sus usuarios y, por tanto, se alinea con sus incentivos.

La federación o fediverse, también es una solución realmente muy convincente. A diferencia de Facebook, Instagram, Whatsup, Discord y Reddit, que son de propiedad y operación centralizada, el software federado requiere que muchos operadores de servidores o nodos independientes que ejecuten instancias que son responsables de decenas o cientos de usuarios. Luego, cada uno de estos servidores usa los protocolos estandarizados para comunicarse entre sí, formando una red social cohesiva y distribuida. Matrix e IRC son protocolos federados, por ejemplo. Otros incluyen Mastodon, que es similar a Twitter en función; PeerTube, para alojar videos y transmisiones en vivo; y Lemmy, que es un equivalente federado de Reddit.

Estas son algunas de las alternativas. Estas plataformas carecen de ese conflicto de interes crucial, que nos está expulsando de las plataformas empresariales. Estos son los hechos: las plataformas abiertas son las únicas que se alinean con los intereses de sus usuarios y las plataformas cerradas explotan a sus usuarios. Una vez que reconozca esto, debeis de  abandonar el barco antes de ser hundido, o de lo contrario está arriesgando su capacidad de organizarse para moverse a otra plataforma. Utilice plataformas abiertas, o de lo contrario, vas a perder. Hazlo hoy.


Recuerdo La ética hacker la cultura hacker subversiva y anti autoritaria luchando por la libertad en las redes y fuera que muchos son los que trabajan a diario en traernos libertad y descentralizacion a(A)a
- ReK2 en el exilio digital

Esto es una traduccion con cambios menores del articulo original de nuestro compa hacker [Drew Devault](https://drewdevault.com/2021/01/28/Use-open-platforms-or-else.html)

⇒ [Este artículo y blog también está disponible en Géminis](geminis://blog.hispagatos.org)
