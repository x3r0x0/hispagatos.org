+++
description = "Help support Hispagatos by mining v2"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "cryptocurrency", "hispagatos"]
title= "Help support Hispagatos by mining v2"
date= 2021-02-20T22:06:07-05:00
+++

# HOW TO - MINE LIKE A GATO
Here is an updated version 2 of our previous quick tutorial on how to help support Hispagatos by running cryptocurrency mining software. The software has changed and the mining ports have changed so we bring you the latest.

## Getting Started

Welcome to a step-by-step Monero (XMR) miner tutorial brought to you by your local gatos y gatas. This was tested with Arch Linux *meow* and updated last on February 20, 2021.

### Installing & Initial Setup

1. Install MoneroOcean's xmrig fork from AUR with paru.
	```shell script
	$ paru xmrig-mo
	```

1. For the best mining optimization, install [msr-tools](https://archlinux.org/packages/community/x86_64/msr-tools/) from AUR with paru.
	```shell script
	$ paru msr-tools
	```

1. The mining information we have is:
	```
	Mining pool:	gulf.moneroocean.stream
	Mining port:	10128
	Mining address: 4AYprKT26fnZYdY1JjKLMR2K7zmCQhe9i5bn5oMV4VtEYvjiFhhXLVq8iycHRLTr6cD3oVSq19Yrx4JfmuuaU453M8GzTKb
	``` 

	**Note: if you have an "Old CPU/GPU" then use port 10032 instead. If it is ancient, then refer to their website help section**

1. Now to configure xmrig.
	```shell script
	$ sudo cp /etc/xmrig-mo/xmrig-mo.conf.example /etc/xmrig-mo/xmrig-mo.conf
	$ sudo vim /etc/xmrig-mo/xmrig-mo.conf
	```

1. Enter the info we have in the config file as follows:
	- Change "url" to our Mining pool:Mining port.
	- Change "user" to the Mining address.
	- Change "pass" to the miner name you want.
	- Keep the other values in "pools" section unchanged.

1. So we would have these values in the config now:
	```
	"url": "gulf.moneroocean.stream:10128",
	"user": "4AYprKT26fnZYdY1JjKLMR2K7zmCQhe9i5bn5oMV4VtEYvjiFhhXLVq8iycHRLTr6cD3oVSq19Yrx4JfmuuaU453M8GzTKb",
	"pass": "killab33z",
	```

	Note: To learn more about how the MoneroOcean pool works, you can visit the [MoneroOcean's FAQ] (https://moneroocean.stream/user/help/faq.html). Their FAQ explains that the pool will auto switch to mine Monero, and other similar coins, and will still yield Monero in the end. RTFM their FAQ for more info.

	Also, on [Reddit] (https://www.reddit.com/r/MoneroMining/comments/f9p3q3/moneroocean_pool_now_support_mining_to_onion/fitv02q/) a MoneroOcean admin states:

	```
	"Algo switching works by modifying miner so it will benchmark algos on your system and provide this algo perf data to the pool, so pool can always give your miner the best profit coin jobs. Pool uses exchange to auto trade all coins to monero to pay it to miners."
	```

1. Set your desired log file location in the config (below with example):
	```
	"log-file": "/tmp/xmrig-mo.log",
	```

1. Now to setup your miner to use a CPU and/or a GPU to mine.

### Setup CPU Mining

For the CPU miner setup, make sure the following is contained in the config:

```
	"cpu": {
		"enabled": true,
		
	"opencl": {
		"enabled": false,
		
	"cuda": { 
		"enabled": false,	
```

### Setup GPU Mining

The following are my findings, but RTFM to be sure:

#### AMD GPU

##### AMD GPU Pre-req

Install the required OpenCL drivers for this to work from AUR:
```shell script
	$ paru opencl-amd
```

##### AMD GPU xmrig-mo.conf changes

For AMD-based GPU, make sure the following is contained in the config:

```
	"opencl": {
		"enabled": true,
		
	"cuda": { 
		"enabled": false,	
```

#### Nvidia GPU

Since I do not have any Nvidia GPUs to test, we look to the [XMRig Configuration Wizard](https://xmrig.com/wizard) to learn what needs to be changed. The following are my findings, but RTFM to be sure:

For Nvidia-based GPU, make sure the following is contained in the config:

```
	"opencl": {
		"enabled": false,
		
	"cuda": { 
		"enabled": true,	
```

### Start the miner

1. Then we use the package's built in user account and systemd script:

	```
	$ sudo systemctl start xmrig-mo.service 
	```

1. Then tail your log file to see the Hash rate or to see if anything failed.

***
### Some troubleshooting

Q1: After running xmrig-mo, the CPU hits 100%! How do I change that?
A1: Try changing the 'max-threads-hint option to 75 instead of the default 100

Q2: With AMD GPU, I get the following error: "disabled (selected OpenCL platform NOT found)"
A2: Make sure you install the pre-req above, the opencl-amd package

***
### And then...

- To see it in the mining pool, go to https://moneroocean.stream/#/dashboard and add the above Mining (XMR) address and you can see your miner and the stats, etc.
- If you need to re-perform xmrig's algorithm performance tester, then start again from the "xmrig-mo.conf.example" file like in this tutorial
- You can also check all the available port numbers which can be used in the Help Section of the [MoneroOcean's website ](https://moneroocean.stream/) as there isn't a separate link anymore.

***
### Anything else...

If there is anything else I missed then let me know. Shouts out to all Hispagatos, all 2600 cats, and all the Fediverse family. And as always -> RTFM, hack the system & enjoy life! - Killab33z ^_^
https://hispagatos.org

