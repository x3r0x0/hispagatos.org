---
title: "Git Send Mail"
date: 2021-04-26T04:31:37+02:00
author: "ReK2"
tags: ["tech", "tecnologia", "hacking", "hackers", "git"]
images: ["https://files.speakerdeck.com/presentations/4f4e51c73f09700022014d32/slide_27.jpg"]
---


## Cómo usar GIT send-email 

Seamos claros, github nos ha mal-educado a muchos usando herramientas de web/UI para compartir parches en nuestros proyectos libres, en realidad git fue desarollado
de manera descentralizada para usarse por medio de smtp y no teniendo por que pasar por una web central. Hay una razon muy clara por la que el kernel "Linux" usa git por medio de listas de correo con smtp.  Git viene con herramientas integradas para colaborar por correo electrónico. Con esta guía, contribuirás a proyectos impulsados por email como el kernel de Linux, PostgreSQL o incluso el mismo git en muy poco tiempo.

# Instalación de GIT send-email

Es probable que ya tengas instalado Git, pero eso no significa que automaticamente Git sepa como y por donde mandar los parches. Puedes verificar si el correo electrónico de envío está disponible ejecutando


```
git send-email --help
```
Si muestra el man page: bien, lo tienes instalado. De lo contrario, debes instalar el git send-email.


### Configuración de tu  nombre y dirección de correo electrónico:

Debes decirle  a Git tu  nombre y dirección de correo electrónico. Probablemente lo hayas hecho ya, pero si no, ejecuta estos comandos:

```
git config --global user.name "mi nombre"
git config --global user.email "myemail@example.com"
```

### Configuración de las opciones de envío de correo

Git send-email envía los correos electrónicos a través de un servidor SMTP, por lo que debes configurar los parámetros del servidor. Consulte la documentación de tu proveedor de correo electrónico para encontrar los parámetros correctos. Así es como configuraría mi configuración de correo:

```
git config --global sendemail.smtpencryption tls
git config --global sendemail.smtpserver mail.hispagatos.org
git config --global sendemail.smtpuser rek2@hispagatos.org
git config --global sendemail.smtpserverport 587
git config --global sendemail.smtppass "AQUI VAMOS A USAR GOPASS/PASS"
```

Como para almacenar la contraseña en el archivo de configuración GIT, obviamente, es un riesgo de seguridad vamos a usar GOPASS. No es obligatorio configurar la contraseña. Si no está configurado, Git send-email te preguntará cada vez que se use el comando.(vamos una lamerada) 



# configurar gopass/git send-email

Buscando y buscando encontre esto sobre [gitcredentials](https://git-scm.com/docs/gitcredentials)
y me quede con el "credential.helper" en el apartado de [Helpers customizados](https://git-scm.com/docs/gitcredentials#_custom_helpers)

Un poco de Linux kung-fu y me quede con esto  si no lo entendeis RTFM el man page y los enlaces que os he puesto arriba.

```
credential.helper=!f() { echo "password=$(gopass show -o -f email/cfernandez@protonmail.ch)"; }; f
```

## Herramientas necesarias

Para aprender mas sobre PASS o GOPASS recomiendo leer la pagina principal de [gopass](https://www.gopass.pw/)
tambien hace tiempo hicimos un tutorial de como installar [Neomutt con GPG](https://hispagatos.org/post/neomutt-gpg-howto-traduccion/) donde usamos pass que es compatible con gopass
para instalar en Arch GNU/Linux

```
paru -S community/gopass
```

Si  teneis protonmail podeis usar en vez del proxy oficial de protonmail que es una mierda, una alternativa llamada [hydroxide](https://github.com/emersion/hydroxide)
en Arch GNU/Linux lo teneis en la AUR

```
paru -S aur/hydroxide
```


### como mandar parches usando git send-email

aqui recomiendo que RTFM el manual con:

```
git send-email --help
```

Pero aqui van unos consejos.

Para no incluirnos a nosotros mismos en el correo que se envia a toda la lista, ponemos suprimirlo con:

```
git config --global sendemail.suppresscc self
```

### Envío de un solo parche

Envío del ultimo commit  en la rama actual:

```
git send-email -1
```

Enviando otro commit:

```
git send-email -1 <referencia del commit>
```


### Envío de varios parches

Envío de los últimos 10 commits en la rama actual:

```
git send-email -10 --cover-letter --annotate
```

La opción --cover-letter crea un correo adicional que se enviará antes de los correos del parche. Puedes escribir una introducción al conjunto de parches en la carta de presentación. Si necesitas explicar los parches, asegúrese de incluir las explicaciones también en los mensajes de confirmación, porque el texto de la carta de presentación no se registrará en el historial de git. Si no crees que sea necesaria ninguna introducción o explicación, está bien con solo el shortlog que se incluye en la carta de presentación de forma predeterminada, y solo establecer el encabezado del "Asunto" en algo sensato.

La opción --annotate hace que se inicie un editor para cada uno de los correos, lo que te permite editar los correos. La opción siempre es necesaria para que puedas editar el encabezado el "Asunto" de la carta de presentación.


### Agregar información sobre la versión del parche

Por defecto, los correos electrónicos de parche tendrán "[PATCH]" en el asunto (o "[PATCH n / m]", donde n es el número de secuencia del parche ym es el número total de parches en el conjunto de parches). Al enviar versiones actualizadas de parches, se debe indicar la versión: "[PATCH v2]" o "[PATCH v2 n / m]". Para hacer esto, use la opción -v. Aquí hay un ejemplo (es posible que desee agregar --annotate para agregar notas al parche sobre lo que cambió en la nueva versión):

```
git enviar-correo electrónico -v2 -1
```

### Cambiar o enmendar la etiqueta [PATCH] en el asunto

La etiqueta "[PATCH]" predeterminada se puede cambiar con --subject-prefix.

Por ejemplo:

- Usar "[PATCH proyecto-destino]" como etiqueta es una buena forma de indicar
- que el parche está destinado al proyecto-destino.

```
git send-email -1 --subject-prefix = "PATCH cookiertfm"
```


### Agregar notas adicionales a los correos electrónicos de parches

A veces es conveniente anotar los parches con algunas notas que no deben incluirse en el mensaje de confirmación. Por ejemplo, uno podría querer escribir "No estoy seguro si esto debería confirmarse todavía, porque ..." en un parche, pero el texto no tiene sentido en el mensaje de confirmación. Estos mensajes se pueden escribir debajo de los tres guiones "---" que se encuentran en cada parche después del mensaje de confirmación. Use la opción --annotate con git send-email para poder editar los correos antes de que se envíen.

### Formatear y enviar en dos pasos

En lugar de usar la opción --annotate, primero se puede ejecutar "git format-patch" para crear archivos de texto (con la opción -o para seleccionar un directorio donde se almacenan los archivos de texto). Estos archivos se pueden inspeccionar y editar, y cuando se hace eso, se puede usar "git send-email" (sin la opción -1) para enviarlos.



Bueno espero que esto os ayude a empezar a usar git con email, y dejar de darle "hits", "page hits" y "visitas a la propaganda del sitio" cuando usais la web.
- recomiendo que instaleis git en vuestro propio servidor pero si no, usar el de [sourcehut](sourcehut.org).
- recomiendo como cliente de email a [aerc](https://aerc-mail.org/)


### Mas lectura:
- [The advantages of an email-driven git workflow](https://drewdevault.com/2018/07/02/Email-driven-git.html)
- [YO'RE using GIT WRONG](https://web.archive.org/web/20180522180815/https://dpc.pw/blog/2017/08/youre-using-git-wrong/)
- [Mailing lists vs Github](https://begriffs.com/posts/2018-06-05-mailing-list-vs-github.html)


### Referencias:
- [git-send-email](https://git-scm.com/docs/git-send-email)
- [Configuring aerc for git via email](https://drewdevault.com/2020/04/20/Configuring-aerc-for-git.html)
- [configuration tool for email + git](https://git-send-email.io/)

ReK2 Mucho amor y lucha.

- gemini://rek2.hispagatos.org
- gemini://blog.hispagatos.org
- https://hispagatos.org
- https://hispagatos.space
- matrix: @rek2:hispagatos.org
- mastodon: @rek2@hispagatos.space

