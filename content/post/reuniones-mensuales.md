+++
description = "Reuniones mensuales"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "Reuniones mensuales"
date= 2019-07-19T2:30:59+02:00
images = [
  "https://external-preview.redd.it/5XIINSlJY3jjjAjwZBg8LIaT8JE_sO_xLQFmIYxC3vU.jpg?auto=webp&s=50dc677476bdee0e0a6b8d38117de40ee465ad00"
] # overrides the site-wide open graph image
+++

# Reuniones mensuales de Hispagatos en el espacio del sindicato anarquista CNT en Alicante, España

<center><img src="/images/a-m-anarchohacker-manifesto-2018-2.png" width="100%" height="100%"></center>

## Qué haremos en estas reuniones:

Hispagatos: Grupo de activistas enfocado en la descentralización, libertad y privacidad en la red.

Objetivos:

- Participar en política a nivel local y nacional para impulsar procesos que promuevan el enfoque del grupo.
- Promover las tecnologías y costumbres de la cultura hacker como la privacidad, el software libre, las aplicaciones federadas o descentralizadas frente a las aplicaciones y servicios centralizados.
- Crear un espacio local donde podamos organizar nuestras acciones políticas y crear herramientas para otros grupos de activistas de justicia social.
- Crear una política anarquista dentro del grupo con participación horizontal, consenso y respeto.

Detalles:

- Todavía estamos ordenando las cosas, y en agosto estaré visitando hackerlabs / hackerspaces y CCC camp 2019 en Alemania, de modo que cuando regresemos podamos comenzar a organizar esto.
- Traiga ideas y buen rollo, BYOD (traiga su propia bebida si quiere beber), refrescos, agua, cerveza, vino, etc.
- Traiga su computadora o portátil para que podamos hackear ideas y llegar a un consenso sobre diferentes temas.
- Cualquiera con espíritu rebelde, desinfluencer, y que apoye la ética hacker y los valores anarquistas es bienvenido.
- Newbees, noobs y lamers son bienvenidos, pero OLVIDE todo lo que aprendió, la mayoria de informacion sobre ciberseguridad, lleva un lavado de la etica hacker y solo prepara para el trabajo en un sistema capitalista, no realmente el hacking y la cultura hacker.
- Todo el que sienta que están en el lado equivocado de la historia, pero que le gustaría cambiar esto, también es bienvenido, pero debe aclararse.
- Policías, Militares, Guardia Civiles o cualquier persona q trabaje para el gobierno u otro tipo de organización totalitaria u opresiva, fascistas, neonazis, capitalistas, imbéciles corporativos, empresarios, influencers. No son bienvenidos.
- Infosec y personas del mundo de la ciber seguridad con el cerebro lavado que trabajan para cualquiera de las fuerzas totalitarias o ganas de trabajar para ellas, no son bienvenidos.

## Más información y para reservar su asistencia.

- [reserva aqui](https://www.meetup.com/es-ES/Hispagatos-descentralizacion-libertad-y-privacidad/events/262967416/)
- [informacion del evento](https://www.meetup.com/es-ES/Hispagatos-descentralizacion-libertad-y-privacidad/)

## Nota: dejaremos meetup.com ya que NO apoyamos esas ideas y compañías, esto es temporal.

- [ReK2](https://keybase.io/rek2)
- [hispagatos.org](https://hispagatos.org)
- [The Anarcho Hacker Manifesto](https://theanarchistlibrary.org/library/anarchohacker-manifesto-2018)
- [Anarchist Hackers](https://anarcho-hacker.info)
