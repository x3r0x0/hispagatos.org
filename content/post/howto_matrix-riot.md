---
title: "Como conectar al servidor Matrix de hispagatos con RIOT"
date: 2019-03-27T00:45:04+01:00
toc: true
draft: false 
images: ["https://fotos.hispagatos.org/_data/i/upload/2018/07/13/20180713204542-d3eb6d12-me.jpg"]
---

# Cómo conectarse al servidor de chat Matrix con el cliente Riot #

# Instalación / Login #
-----------------------

### Riot-Web ###

- Accede a la página de [Riot](https://about.riot.im/)
- Haz click en "Get started"

![GetStarted1](/images/GetStarted1.png)

- Haz click en la imagen "LAUNCH NOW ON YOUR BORWSER"

![RiotWeb1](/images/RiotWeb1.png)

- Crea una cuenta pulsando el botón "Create Account"

![RiotWeb2](/images/RiotWeb2.png)

- Antes de registrarte como tal, dirígete a la categoría "Advanced" y haz click en "Other"

![GetStarted6](/images/GetStarted6.png)

- En "Homeserver URL", cambia "https://matrix.org" por "https://matrix.hispagatos.org" y pulsas "Next"

![GetStarted7](/images/GetStarted7.png)

- Ahora si, regístrate. Pon tus credenciales y pulsa "Register"

![GetStarted8](/images/GetStarted8.png)

- Ya estas dentro!! Envia un mensaje a @rek2 en el canal #intro para que te meta a #hispagatos y demás





### Riot-Desktop ###
GNU/Linux basado en Debian.

- Accede a la página de [Riot](https://about.riot.im/)
- Haz click en "Get started"

![GetStarted1](/images/GetStarted1.png)

- Haz click en la imagen "DOWNLOAD APP FOR DESKTOP"

![GetStarted2](/images/GetStarted2.png)

- Abre la terminal e introduce los comandos uno por uno

![GetStarted3](/images/GetStarted3.png)

```shell
	sudo sh -c "echo 'deb https://riot.im/packages/debian/ bionic main' > /etc/apt/sources.list.d/matrix-riot-im.list"
```
```shell
	curl -L https://riot.im/packages/debian/repo-key.asc | sudo apt-key add -
```
```shell
	sudo apt-get update && sudo apt-get -y install riot-web
```

- Abre la aplicación Riot

![GetStarted4](/images/GetStarted4.png)

- Ahora haces click en "Create account" para crear una nueva cuenta

![GetStarted5](/images/GetStarted5.png)

- Antes de agregar las credenciales, haz click en "Other" de la categoría "Advanced" 

![GetStarted6](/images/GetStarted6.png)

- En "Homeserver URL", cambia "https://matrix.org" por "https://matrix.hispagatos.org" y pulsas "Next"

![GetStarted7](/images/GetStarted7.png)

- Ahora si, regístrate. Pon tus credenciales y pulsa "Register"

![GetStarted8](/images/GetStarted8.png)

- Ya estas dentro!! Envia un mensaje a @rek2 en el canal #intro para que te meta a #hispagatos y demás





### Riot-Android ###

- Descarga Riot
- Abre la aplicación
- Selecciona la casilla "Utilizar opciones personalizadas..."
- En "URL del Servidor Doméstico", cambia "http://matrix.org" por "http://matrix.hispagatos.org"

![AndroidServidor1](/images/AndroidServidor1.png)

- Regístrate. Pon tus credenciales y pulsa el botón "REGISTRAR"

![AndroidServidor2](/images/AndroidServidor2.png)


[//]:===================================================================================================================


# Verificar Claves #
--------------------
###### Riot-Web / Riot-Desktop ######
Para poder leer los mensajes protegidos de los otros usuarios debes tener sus claves, de lo contrario te
será imposible leerlos. A continuación veremos como obtener las claves de los otros miembros para poder leer
sus mensajes.

- Abre la lista de "Members" y haz click sobre el miembro al cual desees verificar las llaves

![Verify0](/images/Verify0.png)

- Busca la palabra haz click sobre la palabra "Verify" 

![Verify1](/images/Verify1.png)

- En "Verify device" pincha en "Begin Verifying"

![Verify2](/images/Verify2.png)

- En la siguiente pestaña saldrá un circulito cargando, esto es para que la otra persona acepte tus llaves pero, como seguramente
cuando estes verificando sus llaves, el usuario no se encuentre conectado, tendrás que hacer click en "Use legacy verification"

![Verify3](/images/Verify3.png)

- Cuando acabe el proceso simplemente habrá que verificar que las claves coinciden pulsando en el botón "I verify that the keys match"

![Verify4](/images/Verify4.png)


[//]:===================================================================================================================


# Key BacKup #
--------------
###### Riot-Web / Riot-Desktop ######
Verificar las llaves cuando hay pocos usuarios en la sala no es un gran problema. Sales, vuelves a entrar y, aún así, en un
momento están todos verificados de nuevo. Esto a la larga se vuelve muy pesado. Para solucionar este problema, Riot nos
ofrece una forma de verificar las llaves automáticamente llamada "Key Backup".

- Haces click en tu nick y después le das a "Settings"

![KeyBackup1](/images/KeyBackup1.png)

- En "Security & Privacy", presiona el botón "Start using Key Backup" en la sección "Key Backup"

![KeyBackup2](/images/KeyBackup2.png)

- Introduce una contraseña bien segura. Usa minúsculas, mayúsculas, números, carácteres especiales ($%\_-@)...

![KeyBackup3](/images/KeyBackup3.png)

- Y luego debes confirmar la contraseña previamente añadida

![KeyBackup4](/images/KeyBackup4.png)

**NOTA** Se recomienda el uso de gestores de contraseñas como [KeePass2](https://keepass.info/) para administrar correctamente
un gran número de contraseñas.

- El código que te devuelva, debes guardarlo (tanto copiándolo a algún sitio, como descargándolo)

![KeyBackup5](/images/KeyBackup5.png)

- En los dos siguientes pasos, básicamente que mantengas en un lugar seguro esa clave y que el proceso ha sido completado satisfactoriamente

![KeyBackup6](/images/KeyBackup6.png)

![KeyBackup7](/images/KeyBackup7.png)

<style>
	img {
		max-width: 100%;
	}
</style>
