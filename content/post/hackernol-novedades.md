---
title: "Hackernol Novedades"
date: 2021-07-09T17:32:56+02:00
draft: false
author: "ReK2"
tags: ["tech", "tecnologia", "hacking", "hackers", "anarquismo", "anarchism", "hacktivism", "social justice", "Justicia social", "privacidad", "privacy", "kolektiva", "odysee", "hispagatos"]
images: ["/images/portada-nueva-hackernol-600-400.png"]
---

### Hackerñol Para Hackers en Español
- **Novedades! Y actualización pendiente**

---

![Logo de Hackerñol](/images/portada-nueva-hackernol-600-400.png)

---

¡Hola! Bueno en este blog de hispagatos solemos enfocarnos más en análisis, tutoriales CTF, etc. Pero hoy quería escribir sobre Hackerñol, que hace tiempo que no decimos nada por estos medios, ya que regularmente los seguidores nos siguen por mastodon, por [kolektiva](https://kolektiva.media/video-channels/hackernol/videos) u odysee. Pero vamos a hacer una excepción.

Como todos sabéis nuestro soporte y apoyo va siempre para [PEERTUBE](https://joinpeertube.org/es/), especialmente a [kolektiva.media](https://kolektiva.media/video-channels/hackernol/videos), ya que son la plataforma ideal a nuestras ideas, pero desgraciadamente vivimos en el capitalismo, y nos hacen falta fondos para subsistir como colectivo y odysee es el menos malo de todos los que dan algo de soporte financiero, desgraciadamente no hay mucha gente de los nuestros en esta plataforma y de por sí por encima asusta, pero si te pones a buscar hay muchos personajes y compas que tienen videos de hacking y Linux que son afines a nuestras ideas.

Bueno empezando con las novedades en este último año en Hackerñol hemos:

- renovado la intro.
- renovada la portada.
- creado el [canal 1](https://odysee.com/@Hackernol:7/Hacker%C3%B1ol:1) que es solo de nuestro show y tutoriales etc.
- creado el [canal 2 de juegos](https://odysee.com/@Hackernol:7/Juegos:8), juegos a los que solemos jugar en GNU/Linux y solo en esta plataforma, nativo o con proton.
- empezando otros canales afines dedicados a:
  - [Cyberpunk](https://odysee.com/@CyberPunk:c)
  - [Anarquismo](https://odysee.com/@Anarchism:9)
  - [Contra el capitalismo de la vigilancia](https://odysee.com/@Surveillance_Capitalism:b)
  - [Traer a odysee nuestro programa favorito de hacking “off the hook”](https://odysee.com/@Off-the-Hook_2600:8)
- Hemos empezado a hacer nuestro show [en vivo y en directo!](https://odysee.com/general:a823be7550f5f4656985c34bf61bd3fd68ce0769)
- Tambien jugamos a RPG de mesa con dados y juegos de ordenador con linux [todo en directo](https://odysee.com/Hacker%C3%B1ol-Juegos:b93a4b950e7184408be911c3b00f95a08f2d757a)

¡Aparte de esto hemos pasado los 500+ seguidores! 
¡Muchas gracias a todos!! Parece poco pero en una plataforma alternativa
como odysee donde no es masivo como el YouTube, es un número considerable. Muchas gracias

Lo que viene:
- Segmentos más cortos pero casi a diario con noticias de hacking, seguridad, análisis etc.
- Seguiremos con nuestro show de 1-3 horas con las caras familiares en las que podemos analizar y opinar más profundamente.
- A ver si por fin se saca el plugin de peertube para que la gente pueda donar y podemos si hay apoyo enfocarnos solo en peertube.


Nos podeis seguir en kolektiva por medio de mastodon, siguiendo al usuario:
- hackernol@kolektiva.media

¡Bueno esto es todo compas!
¡Un abrazo, mucho amor y en la lucha!

ReK2
- Mastodon: @rek2@hispagatos.space
- Matrix: @rek2:hispagatos.org
- Gemini: Gemini://rek2.hispagatos.org
