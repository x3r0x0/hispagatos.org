+++
description = "NeoMutt GNUPG y Pass"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "Neomutt Gpg Howto Traduccion"
date= 2019-02-03T23:44:40-03:00
images = [
  "https://gitlab.com/uploads/-/system/project/avatar/4868880/m2.png"
] # overrides the site-wide open graph image
+++

### NEOMUTT, GNUPG y Pass

* Vamos a instalar neomutt una consola moderna y cliente configurable imap/pop3/smtp.
* Vamos a usar GNUPG para descifrar y encriptar.
* Vamos a usar el Pasa para guardar la contraseña de neomutt.



- [Guia NeoMutt](https://neomutt.org/guide/)
- [Documentación GNUpg](https://www.gnupg.org/)
- [Documentación Pass](https://www.passwordstore.org/)


Bien, ensuciémonos las manos.

## Instalación

Estoy usando BlackArch [BlackArch GNU/Linux](https://blackarch.org/) con **yay** pero ustedes pueden usar **pacman**

```
yay -S neomutt
yay -S gnupg
yay -S pass
```

```
mkdir ~/.config/neomutt
cd ~/.config/neomutt
```
```
sudo updatedb
locate neomuttrc
cp /etc/neomuttrc ~/.config/neomutt/
```

Antes de empezar, asegúrate de tener tu gnupg listo, es decir, todos tus amigos y contactos.
claves públicas y tú clave privada con [onekey](https://onlykey.io/) configurada si tienes una.
Si estás usando [wayland](https://wayland.freedesktop.org/) y [sway](https://swaywm.org/), te recomiendo configurar [dmenu](https://wiki.archlinux.org/index.php/dmenu) para que te solicite tú contraseña.

Para ver una lista de plugins y opciónes de dmenú:
```
yay -Ss dmenu
```
Ahora vamos a comprobar el  **key ID**
```
gpg -k
gpg -K
```
esto listará tus claves públicas y secretas en ese orden.

```
locate colors.linux
cp /usr/share/doc/neomutt/samples/colors.linux ~/.config/neomutt/
```
```
locate gpg.rc
cp /usr/share/doc/neomutt/samples/gpg.rc ~/.config/neomutt/
```
```
locate mailcap
cp /etc/mailcap ~/.config/neomutt/
```

## Configuración
Edita ~/.config/neomutt/neomuttrc

aquí hay un ejemplo de configuración:
```
~/.config/neomutt/neomuttrc
# ejecutamos source en nuestro color theme 

source ~/.config/neomutt/colors.linux

# Podemos ejecutar el comando source a tantos archivos como sea necesario si deseamos dividir las piezas de configuración por archivos en  
#de tener un gran arhcivo de configuración
#source ~/.config/neomutt/sidebar

# Aquí ejecutamos el gestor de contraseñas local y obtenemos la contraseña para imap y smtp en una variable mutt
set mailcap_path = ~/.config/neomutt/mailcap
set my_pass="`pass neomutt/rek2`"

# Aquí están los ajustes generales
set realname="ReK2"
set from="rek2@hispagatos.org"
set mail_check=90
set timeout=15


# IMAP
set imap_user="rek2@hispagatos.org"
set imap_pass=$my_pass
set folder=imaps://rek2@hispagatos.org
set spoolfile="+INBOX"

set sort=reverse-date
set sort_aux=last-date-received

#sidebar
set sidebar_visible = no
set sidebar_format = "%B%* %n"
set mail_check_stats
set sidebar_new_mail_only = no
set sidebar_width = 15
set sidebar_short_path = no

# --------------------------------------------------------------------------
# Nota: Todas las operaciones de color son de la forma:
#       color OBJECT FOREGROUND BACKGROUND
# Color del buzón de correo actual, abierto
# Nota: Esta es una opción general de NeoMutt que colorea todos los elementos seleccionados.
#color indicator cyan white
# Color del buzón de correo resaltado, pero no abierto.
color sidebar_highlight white color8
# Color del separador que separa la barra lateral de los paneles NeoMutt
color sidebar_divider color8 white
# Color para dar a los buzones de correo que contienen correo marcado
color sidebar_flagged red white
# Color para dar a los buzones de correo que contienen correo nuevo
color sidebar_new green white
# Color para dar a los buzones de correo que no contengan correo nuevo o marcado, etc.
color sidebar_ordinary color245 default


# SMTP
set smtp_url="smtps://rek2@hispagatos.org:465"
set smtp_pass=$my_pass

# Source GNUPG settings to encrypt/decrypt/sign email
source ~/.config/neomutt/gpg.rc

auto_view text/html
```

Puedes personalizar la configuración y los colores, pero las piezas más importantes son las siguientes
es configurar las  opciones imap y smtp.


NOTA: Túconfiguración  SMTP y IMAP/POP3 puede ser diferente.
Haz clic en el enlace anterior para acceder a la documentación.


Creamos una variable que va a canalizar la contraseña desde un gestor local de contraseñas encriptado
llamado pase




## CONFIGURANDO PASS

Si aún no ha importado o creado tu par de claves (privadas y públicas), hazlo ahora.
En caso de que las estás moviendo, asegúrate de importarlas

```
gpg --import pgp-priv
gpg --import pgp-public
```

Ahora haz una lista de tus claves públicas necesitarás obtener la identificación de tú clave personal.
la que quieras usar para firmar y descifrar.

```
gpg -k
```

Un ejemplo será el correo electrónico asociado con la clave como:

rek2@hispagatos.org

Entonces, con esa información que tienes ejecuta:

```
pass init rek2@hispagatos.org
```
Esto inicializa la base de datos de contraseñas cifradas.

Ahora tenemos que confiar en la llave para poder usarla.
así que hazlo con tu clave de identificación.

```
gpg --edit-key rek2@hispagatos.org
```
Una vez adentro escribe:

trust
5
y

Eso es todo,ahora es confiable.

A continuación, debes escribir tus credenciales, nombre o como quieras llamarle,
**más tarde necesitarás esto** en la configuración de neomutt para recuperar la contraseña
A continuación se te pedirá que guardes la contraseña 2 veces... Házlo :)

```
pass insert neomutt/rek2
```
Bien!, ahora valida la base de datos con
```
pass neomutt/rek2
```
Deberías ver tu nueva entrada :)

RTFM para cómo usar pass es muy útil para muchas otras aplicaciones.

---
### CONFIGURAR GPG.RC

Aquí tengo un ejemplo de mi configuración, Por supuesto cambié mi información
y agregó una cadena aleatoria s pgp key, DebeS introducir aquí tú clave pgp
que quieras usar por defecto, hay otros comandos que puedes obtener desde la
documentación para añadir claves de diferenciación, etc.

~/.config/neomutt/gpg.rc

```
set pgp_default_key="FF095ACE1717888D5F81618BCE0E532EB65503DD"
set pgp_decode_command="gpg --status-fd=2 %?p?--pinentry-mode loopback --passphrase-fd 0? --no-verbose --quiet --batch --output - %f"
set pgp_verify_command="gpg --status-fd=2 --no-verbose --quiet --batch --output - --verify %s %f"
set pgp_decrypt_command="gpg --status-fd=2 %?p?--pinentry-mode loopback --passphrase-fd 0? --no-verbose --quiet --batch --output - --decrypt %f"
set pgp_sign_command="gpg %?p?--pinentry-mode loopback --passphrase-fd 0? --no-verbose --batch --quiet --output - --armor --textmode %?a?--local-user %a? --detach-sign %f"
set pgp_clearsign_command="gpg %?p?--pinentry-mode loopback --passphrase-fd 0? --no-verbose --batch --quiet --output - --armor --textmode %?a?--local-user %a? --clearsign %f"
set pgp_encrypt_only_command="/usr/lib/neomutt/pgpewrap gpg --batch --quiet --no-verbose --output - --textmode --armor --encrypt -- --recipient %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/neomutt/pgpewrap gpg %?p?--pinentry-mode loopback --passphrase-fd 0? --batch --quiet --no-verbose --textmode --output - %?a?--local-user %a? --armor --sign --encrypt -- --recipient %r -- %f"
set pgp_import_command="gpg --no-verbose --import %f"
set pgp_export_command="gpg --no-verbose --armor --export %r"
set pgp_verify_key_command="gpg --verbose --batch --fingerprint --check-sigs %r"
set pgp_list_pubring_command="gpg --no-verbose --batch --quiet --with-colons --with-fingerprint --with-fingerprint --list-keys %r"
set pgp_list_secring_command="gpg --no-verbose --batch --quiet --with-colons --with-fingerprint --with-fingerprint --list-secret-keys %r"
set pgp_good_sign="^\\[GNUPG:\\] GOODSIG"
set pgp_check_gpg_decrypt_status_fd
set pgp_use_gpg_agent = yes
set pgp_self_encrypt = yes
set crypt_autosign = yes
set crypt_autoencrypt = yes
set crypt_replyencrypt = yes
```

---
OK, en cada sesión tendrás que introducir la contraseña de tu clave privada gpg para abrir
tu base de datos de contraseñas "pass" porque pass pide  la clave gnupg para abrir la base de datos.
Sin tu clave pgp no podrás abrir pass por lo que te recomiendo usar
un dispositivo criptográfico externo como onlykey, yubikey, etc,
esto, no es necesario pero de hacerlo así tendrás tus llaves en tu ordenador....
Recuerda que esta es sólo una manera muy superficial de conseguir hacer trabajar estas tres
herramientas en conjunto, depende de tí convertirte en un usuario avanzado y RTFM
para disfrutar realmente de las cosas que puedes hacer con ellas.
Disfruta y por favor, ponte en contacto conmigo para corrección de errores tipográficos/erratas y gramática.


ReK2 in Keybase

- [ReK2 in Keybase](https://keybase.io/rek2)
- [HISPAGATOS](https://hispagatos.org)


HISPAGATOS


Traducción por Sigma0100
