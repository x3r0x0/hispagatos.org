#/bin/bash

# run hugo
hugo
sleep 1

# rsync
rsync -avz -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p 2200" --progress public/gemini/ rek2@blog.hispagatos.org:/srv/gemini/blog.hispagatos.org/
# casa
rsync -avz -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress public/gemini/ rek2@blog.hispagatos.org:/srv/gemini/blog.hispagatos.org/
